//
//  DishTest.swift
//  ZhapFan
//
//  Created by Chan Jing Hong on 7/30/15.
//  Copyright (c) 2015 Ken. All rights reserved.
//

import UIKit
import XCTest
import ZhapFan

class DishTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }
    
    func testScoopsMustBeMoreThan0() {
        var chicken:Dish = Dish(dishName: "Chicken")
        chicken.numberOfScoops = -10
        
        XCTAssert(chicken.numberOfScoops >= 0, "There is no negative chicken")
    }
    
    func testScoopsMustBeLessThan3() {
        var chicken:Dish = Dish(dishName: "Chicken")
        chicken.numberOfScoops = 99999
        
        XCTAssert(chicken.numberOfScoops <= 3, "Chicken Cannot be more than 3")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
