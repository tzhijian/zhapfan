//
//  Order.swift
//  ZhapFan
//
//  Created by Jun Yee Sin on 7/30/15.
//  Copyright (c) 2015 Ken. All rights reserved.
//

import UIKit

public class Order: NSObject {
   
    public var order : String
    public var dishes: [Dish] = []
    
    public func placedOrder() {
        
    }
    
    init(order : String) {
        self.order = order
        super.init()
    }

    
}
