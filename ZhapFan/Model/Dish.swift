//
//  Dish.swift
//  ZhapFan
//
//  Created by Chan Jing Hong on 7/30/15.
//  Copyright (c) 2015 Ken. All rights reserved.
//

import Foundation

public class Dish:NSObject {
    
    public var dishName:String
    public var numberOfScoops:Int = 0 {
        didSet {
            if numberOfScoops > 3 {
                numberOfScoops = 3
            } else if numberOfScoops < 0 {
                numberOfScoops = 0
            }
        }
    }
    
    public init(dishName:String) {
        self.dishName = dishName
        super.init()
    }
    
    
    
}


