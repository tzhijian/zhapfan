//
//  MenuTableViewController.swift
//  ZhapFan
//
//  Created by Chan Jing Hong on 7/30/15.
//  Copyright (c) 2015 Ken. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {

    var foods = ["Rice", "CookedChicken", "Vegetables", "Noodle"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return foods.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("foodcell", forIndexPath: indexPath) as! UITableViewCell
        
        // So i did this instead
        var myView = UIView(frame: CGRectMake(20, 20, 277, 58))
        myView.backgroundColor = UIColor.blueColor()
        
        cell.backgroundView = myView
        cell.textLabel?.backgroundColor = UIColor(patternImage: UIImage(named: "\(foods[indexPath.row]).jpg")!)
        cell.textLabel?.text = foods[indexPath.row]
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var selectedFood = foods[indexPath.row]
        
        if (selectedFood == "Rice") {
            
        } else if (selectedFood == "CookedChicken") {
            
        } else if (selectedFood == "Vegetables") {
            
        } else if (selectedFood == "Noodle") {
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
