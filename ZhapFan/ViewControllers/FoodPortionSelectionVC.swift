//
//  FoodPortionSelectionVC.swift
//  ZhapFan
//
//  Created by Ken on 7/29/15.
//  Copyright (c) 2015 Ken. All rights reserved.
//

import UIKit

class FoodPortionSelectionVC: UIViewController {

    @IBOutlet var foodImage: UIImageView!
    @IBOutlet var foodNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        foodImage.backgroundColor = UIColor.blackColor()
        foodNameLabel.backgroundColor = UIColor.redColor()
        
        var zhapfan: Ricebox = Ricebox(DishesNum: 6)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
